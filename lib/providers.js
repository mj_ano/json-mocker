'use strict';

let _ = require('lodash');
let moment = require('moment');

let providers = [
  {
    type: 'boolean',
    data: [true, false]
  },

  {
    type: 'date',
    getter: function (from, to, format) {
      let d = new Date(_.random(from, to), _.random(0, 11), _.random(1, 31));
      let month = d.getMonth() + 1;

      return moment(d).format(format || 'YYYY-MM-DD');
    },
    noCache: true
  },

  {
    type: 'integer',
    getter: _.random,
    noCache: true
  },

  {
    type: 'maleFirstName',
    data: ["Adam", "Adrian", "Alan", "Alexander", "Andrew", "Anthony", "Austin", "Benjamin", "Blake", "Boris", "Brandon", "Brian", "Cameron", "Carl", "Charles", "Christian", "Christopher", "Colin", "Connor", "Dan", "David", "Dominic", "Dylan", "Edward", "Eric", "Evan", "Frank", "Gavin", "Gordon", "Harry", "Ian", "Isaac", "Jack", "Jacob", "Jake", "James", "Jason", "Joe", "John", "Jonathan", "Joseph", "Joshua", "Julian", "Justin", "Keith", "Kevin", "Leonard", "Liam", "Lucas", "Luke", "Matt", "Max", "Michael", "Nathan", "Neil", "Nicholas", "Oliver", "Owen", "Paul", "Peter", "Phil", "Piers", "Richard", "Robert", "Ryan", "Sam", "Sean", "Sebastian", "Simon", "Stephen", "Steven", "Stewart", "Thomas", "Tim", "Trevor", "Victor", "Warren", "William"]
  },

  {
    type: 'femaleFirstName',
    data: ["Abigail", "Alexandra", "Alison", "Amanda", "Amelia", "Amy", "Andrea", "Angela", "Anna", "Anne", "Audrey", "Ava", "Bella", "Bernadette", "Carol", "Caroline", "Carolyn", "Chloe", "Claire", "Deirdre", "Diana", "Diane", "Donna", "Dorothy", "Elizabeth", "Ella", "Emily", "Emma", "Faith", "Felicity", "Fiona", "Gabrielle", "Grace", "Hannah", "Heather", "Irene", "Jan", "Jane", "Jasmine", "Jennifer", "Jessica", "Joan", "Joanne", "Julia", "Karen", "Katherine", "Kimberly", "Kylie", "Lauren", "Leah", "Lillian", "Lily", "Lisa", "Madeleine", "Maria", "Mary", "Megan", "Melanie", "Michelle", "Molly", "Natalie", "Nicola", "Olivia", "Penelope", "Pippa", "Rachel", "Rebecca", "Rose", "Ruth", "Sally", "Samantha", "Sarah", "Sonia", "Sophie", "Stephanie", "Sue", "Theresa", "Tracey", "Una", "Vanessa", "Victoria", "Virginia", "Wanda", "Wendy", "Yvonne", "Zoe"]
  },

  {
    type: 'firstName',
    getter: function () {
      return this.gender() === 'M' ? this.maleFirstName() : this.femaleFirstName();
    }
  },

  {
    type: 'lastName',
    data: ["Abraham", "Allan", "Alsop", "Anderson", "Arnold", "Avery", "Bailey", "Baker", "Ball", "Bell", "Berry", "Black", "Blake", "Bond", "Bower", "Brown", "Buckland", "Burgess", "Butler", "Cameron", "Campbell", "Carr", "Chapman", "Churchill", "Clark", "Clarkson", "Coleman", "Cornish", "Davidson", "Davies", "Dickens", "Dowd", "Duncan", "Dyer", "Edmunds", "Ellison", "Ferguson", "Fisher", "Forsyth", "Fraser", "Gibson", "Gill", "Glover", "Graham", "Grant", "Gray", "Greene", "Hamilton", "Hardacre", "Harris", "Hart", "Hemmings", "Henderson", "Hill", "Hodges", "Howard", "Hudson", "Hughes", "Hunter", "Ince", "Jackson", "James", "Johnston", "Jones", "Kelly", "Kerr", "King", "Knox", "Lambert", "Langdon", "Lawrence", "Lee", "Lewis", "Lyman", "MacDonald", "Mackay", "Mackenzie", "MacLeod", "Manning", "Marshall", "Martin", "Mathis", "May", "McDonald", "McLean", "McGrath", "Metcalfe", "Miller", "Mills", "Mitchell", "Morgan", "Morrison", "Murray", "Nash", "Newman", "Nolan", "North", "Ogden", "Oliver", "Paige", "Parr", "Parsons", "Paterson", "Payne", "Peake", "Peters", "Piper", "Poole", "Powell", "Pullman", "Quinn", "Rampling", "Randall", "Rees", "Reid", "Roberts", "Robertson", "Ross", "Russell", "Rutherford", "Sanderson", "Scott", "Sharp", "Short", "Simpson", "Skinner", "Slater", "Smith", "Springer", "Stewart", "Sutherland", "Taylor", "Terry", "Thomson", "Tucker", "Turner", "Underwood", "Vance", "Vaughan", "Walker", "Wallace", "Walsh", "Watson", "Welch", "White", "Wilkins", "Wilson", "Wright", "Young"]
  },

  {
    type: 'email',
    getter: function () {
      return (this.firstName() + '.' + this.lastName() + '@' + this.company() + '.com').toLowerCase();
    }
  },

  {
    type: 'streetPartI',
    data: ["Ninth", "Washington", "Lafayette", "Wacker", "Bogardus", "LaSalle", "Greenwich", "Shubert", "Frederick", "Riverside", "Foster", "Commissioners", "Howard", "Bank", "Taras", "Forsyth", "Leonard", "Diversey", "Henry", "Varick", "Ohio", "Magnificent", "Oak", "Blue", "Bridge", "Fulton", "Cherry", "Stuyvesant", "Houston", "Third", "Columbus", "Chambers", "Ogden", "Division", "Paseo", "Thompson", "Torrence", "West", "Jarvis", "Thirteenth", "Pearl", "Logan", "Armitage", "Rivington", "Mott", "Vesey", "Dyer", "Mid", "Spruce", "Hubbard", "Sheridan", "George", "Lenox", "Weehawken", "Spring", "First", "Chrystie", "Elizabeth", "Stone", "Roosevelt", "Duke", "Jones", "Stanton", "Sullivan", "Cicero", "DeKoven", "Coenties", "Broome", "Pershing", "Archer", "Claremont", "Sheffield", "Delancey", "Waverly", "Chicago", "Patchin", "Sixth", "Worth", "Tenth", "Vanderbilt", "William", "Gay", "Lexington", "Center", "Centre", "Garfield", "Park", "Cooper", "Plaza", "Dey", "Devon", "State", "Franklin", "Ridge", "Lincoln", "Halsted", "Bowery", "Cermak", "Wall", "Morningside", "MacDougal", "Michigan", "Manhattan", "Randolph", "Ludlow", "Baxter", "Elston", "Rush", "Bond", "Harlem", "Grand", "Six", "Chatham", "Bleecker", "Pulaski", "Fullerton", "Belmont", "Pleasant", "Essex", "Stony", "Cabrini", "Mulberry", "Lake", "Second", "Congress", "South", "Liberty", "Hester", "Great", "Doyers", "Wells", "Sesame", "Church", "Sylvan", "Kedzie", "Eleventh", "Dyckman", "North", "Nassau", "Fifth", "Midway", "Milwaukee", "Broadway", "Orchard", "Canal", "Seventh", "Whitehall", "Maxwell", "Madison", "Dempster", "Western", "Prairie", "Fort", "Clark", "Touhy", "Maiden", "Jeffery", "Wooster", "East", "Gansevoort", "Marketfield", "Addison", "Hudson"]
  },

  {
    type: 'streetPartII',
    data: ["Court", "Station", "Circle", "Street", "Place", "Avenue", "Square", "Road", "Terrace", "Drive", "Parkway", "Lane", "Plaza", "Boulevard", "Walk", "Alley", "Way", "Highway"]
  },

  {
    type: 'street',
    getter: function () {
      return _.random(1, 500)+ ' ' + this.streetPartI() + ' ' + this.streetPartII();
    }
  },

  {
    type: 'location',
    data: require('./store/location.json')
  },

  {
    type: 'city',
    getter: function () {
      let location = this.util.getValueById('location', this.location());

      return location.city.split(/\s+/).map(city => _.capitalize(city)).join(' ');
    }
  },

  {
    type: 'state',
    getter: function () {
      let location = this.util.getValueById('location', this.location());
      return location.state;
    }
  },

  {
    type: 'company',
    data: ["Aarau", "Aarbrix", "Adlistica", "Aesch", "Affoltern", "Aigle", "Allschtica", "Altman", "Amristica", "Appenzell", "Arbon", "Arlesheim", "Ascona", "Aubonne", "Avenches", "Bassersman", "Bellinzona", "Biasca", "Binningen", "Birsfelden", "Bischofszell", "Bremgarten", "Bussigny", "Carouge", "Chiasso", "Conthey", "Coppet", "Cossonay", "Croglio", "Crissier", "Cudrefin", "Diessenhofen", "Dietikon", "Ebikon", "Ecublens", "Eglittorn", "Einsiedeln", "Erlach", "Estavayer", "Flagrain", "Frauenfeld", "Freienline", "Fribourg", "Gordola", "Grandcour", "Herittorn", "Hermance", "Huttgrain", "Ilanz", "Interlaken", "Kaisers", "Klingnau", "Kloten", "Kreuzlingen", "Kriens", "Langenthal", "Lausanne", "Lenz", "Lichtensteig", "Liestal", "Locarno", "Losone", "Lugano", "Lutry", "Martigny", "Meilen", "Mellingen", "Mendrisio", "Meyrittornontreux", "Morcote", "Morges", "Moudon", "Moutier", "Murten", "Muttenz", "Neuhausen", "Neunkirch", "Obergrain", "Oftringen", "Opfikon", "Orbe", "Ostermundigen", "Payerne", "Porrentruy", "Pratteln", "Prilly", "Rapperdot", "Regensbrix", "Regensman", "Reinach", "Renens", "Rheinau", "Rheineck", "Rheinfelden", "Richterdot", "Riehen", "Rolle", "Romanshorn", "Romont", "Rorschach", "Saillon", "Sargans", "Sarnen", "Schaffhausen", "Schlieren", "Schwyz", "Sembrancher", "Sempach", "Sierre", "Sionicon", "Solothurn", "Spiez", "Spreitenline", "Steckborn", "Steffis", "Steinhausen", "Sursee", "Thalcat", "Thusis", "Unterseen", "Urman", "Uznach", "Uzcat", "Valangin", "Vernier", "Versoix", "Villeneuve", "Villars", "Walenstadt", "Wallisellen", "Wangen", "Werdenbrix", "Weinfelden", "Wettingen", "Wetzikon", "Wiedlisline", "Willittorn", "Winterthur", "Zerman", "Zofingen", "Zurzachnest"]
  },

  {
    type: 'phone',
    getter: function () {
      return +(_.map(_.range(10), function (i) {
        return _.random(i === 0 ? 1 : 0, 9);
      }).join(''));
    }
  },

  {
    type: 'title',
    data: require('./store/title.json')
  },

  {
    type: 'salary',
    data: _.range(3000, 18750, 750)
  },

  {
    type: 'ratings',
    data: _.range(1, 5.1, .1).map(function(item) { return +item.toFixed(1);})
  },

  {
    type: 'currency',
    data: require('./store/currency.json'),
    key: 'code'
  },

  {
    type: 'lorem',
    noCache: true,
    getter: function (input) {
      let data = ['lorem', 'ipsum', 'dolor', 'sit', 'amet', 'nusquam', 'adolescens', 'cum', 'in', 'iudico', 'admodum', 'qui', 'no', 'vel', 'ei', 'nemore', 'interpretaris', 'usu', 'fugit', 'atomorum', 'eu', 'brute', 'tritani', 'voluptua', 'id', 'his', 'pro', 'at', 'aliquip', 'disputando', 'reprehendunt', 'vim', 'commodo', 'integre', 'democritum', 'ex', 'quo', 'omittam', 'offendit', 'ut', 'cu', 'iusto', 'impedit', 'definiebas', 'agam', 'menandri', 'definitiones', 'pri', 'vix', 'ad', 'qualisque', 'scribentur', 'eloquentiam', 'maiorum', 'reformidans', 'has', 'tollit', 'intellegat', 'nec', 'ludus', 'lucilius', 'an', 'antiopam', 'repudiare', 'sea', 'mazim', 'viris', 'invenire', 'congue', 'ne', 'duo', 'dissentiunt', 'nullam', 'incorrupte', 'ius', 'graeci', 'consulatu', 'constituto', 'eam', 'tamquam', 'appetere', 'eos', 'vituperata', 'cotidieque', 'adversarium', 'nam', 'copiosae', 'liberavisse', 'te', 'eum', 'veri', 'phaedrum', 'legendos', 'per', 'omnesque', 'molestie', 'deseruisse', 'stet', 'habeo', 'tempor', 'labore', 'mel', 'errem', 'nostrum', 'graecis', 'delectus', 'ea', 'zril', 'dolore', 'vidisse', 'tation', 'affert', 'verear', 'praesent', 'nonumes', 'est', 'elitr', 'tractatos', 'posidonium', 'gloriatur', 'audire', 'adipiscing', 'facer', 'accusata', 'quas', 'postulant', 'et', 'consul', 'insolens', 'prodesset', 'purto', 'detraxit', 'sumo', 'causae', 'tota', 'nominavi', 'periculis', 'legere', 'mea', 'consectetuer', 'elit', 'aenean', 'ligula', 'eget', 'massa', 'sociis', 'natoque', 'penatibus', 'magnis', 'dis', 'parturient', 'montes', 'nascetur', 'ridiculus', 'mus', 'donec', 'quam', 'felis', 'ultricies', 'pellentesque', 'pretium', 'quis', 'sem', 'nulla', 'consequat', 'enim', 'pede', 'justo', 'fringilla', 'aliquet', 'vulputate', 'arcu', 'rhoncus', 'imperdiet', 'a', 'venenatis', 'vitae', 'dictum', 'mollis', 'integer', 'tincidunt', 'cras', 'dapibus', 'vivamus', 'elementum', 'semper', 'nisi', 'eleifend', 'tellus', 'leo', 'porttitor', 'ac', 'aliquam', 'ante', 'viverra', 'feugiat', 'phasellus', 'metus', 'varius', 'laoreet', 'quisque', 'rutrum', 'etiam', 'augue', 'curabitur', 'ullamcorper', 'dui', 'maecenas', 'tempus', 'condimentum', 'libero', 'neque', 'sed', 'nunc', 'blandit', 'luctus', 'pulvinar', 'hendrerit', 'odio', 'sapien', 'faucibus', 'orci', 'eros', 'duis', 'mauris', 'nibh', 'sodales', 'sagittis', 'magna', 'bibendum', 'velit', 'cursus'];
      let parsed = /(\d*)([wsp]?)/.exec(input);
      let count = parsed[1] || 10;
      let format = parsed[2] || 'w';

      let word = (count) => _.map(_.range(count), () => _.nth(data, _.random(data.length - 1))).join(' ');
      let sentence = (count) => _.map(_.range(count), () => _.upperFirst(word(_.random(5, 10)))).join('. ') + '.';
      let paragraph = (count) => _.map(_.range(count), () => sentence(_.random(3, 8))).join('\n');

      return format === 'w' ? word(count) : (format === 's' ? sentence(count) : paragraph(count));
    }
  }
];

let add = (provider) => providers.push(provider);

let all = () => providers;

let provider = (type) => _.find(providers, {type: type});

let data = (type) => provider(type).data;

let random = (type) => _.sample(data(type));

let getValueById = (type, id) => _.find(data(type), {_id: id});

module.exports = {
  add: add,
  all: all,
  data: {
    random: random,
    getValueById: getValueById
  }
};