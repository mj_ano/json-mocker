'use strict';

let DataItem = require('./data-item');

module.exports = {
  build: require('./parse-build'),
  dataItem: (index) => new DataItem(index),
  provider: {
    add: require('./providers').add
  }
};
