'use strict';

let _ = require('lodash');
let providers = require('./providers');

function defaultGetter(input) {
  let type = input.type;
  let getter = input.getter;
  let key = input.key || '_id';
  let flag = input.flag;
  let noCache = input.noCache;

  return function () {
    let args = Array.prototype.slice.call(arguments);
    let randomValue = (typeof getter === 'function') ? getter.apply(this, args) : this.util.random(type);
    return this.set(type, (flag ? (randomValue[key] ? randomValue[key] : randomValue) : randomValue), noCache);
  };
}

function isArrayOfObjects(input) {
  return _.isArray(input) ? _.isPlainObject(input[0]) : false;
}

function addApiMethod(provider) {
  DataItem.prototype[provider.type] = defaultGetter({
    type: provider.type,
    getter: provider.getter,
    key: provider.key,
    flag: isArrayOfObjects(provider.data),
    noCache: provider.noCache
  });
}

function DataItem (index) {
  this._data = {
    index: index,
    gender: _.sample(['M', 'F'])
  };

  _.forEach(providers.all(), addApiMethod);
}

DataItem.prototype = {
  set: function (type, value, noCache) {
    if (noCache) {
      return value;
    }
    this._data[type] = this._data[type] || value;
    return this._data[type];
  },

  data: function () {
    return this._data;
  },

  util: {
    random: (type) => providers.data.random(type),
    getValueById: (type, id) => providers.data.getValueById(type, id)
  },

  index: function () {
    return this.data().index;
  },

  gender: function () {
    return this.data().gender;
  },

  sibling: function () {
    return _.sample(this.siblings(1, true));
  },

  siblings: function (max, noEmptyList) {
    max = +max || 5;
    return _.sampleSize(_.without(_.range.apply(null, DataItem.indexRange), this.index()), _.random((noEmptyList ? 1 : 0), max));
  }
};

module.exports = DataItem;