'use strict';

let _ = require('lodash');
let DataItem = require('./data-item');

// Return array of string values, or NULL if CSV string not well formed.
// Taken from http://stackoverflow.com/questions/8493195/how-can-i-parse-a-csv-string-with-javascript-which-contains-comma-in-data
function CSVtoArray(text) {
  var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
  var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
  // Return NULL if input string is not well formed CSV string.
  if (!re_valid.test(text)) return null;
  var a = []; // Initialize array to receive values.
  text.replace(re_value, // "Walk" the string using replace with callback.
    function(m0, m1, m2, m3) {
      // Remove backslash from \' in single quoted values.
      if (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
      // Remove backslash from \" in double quoted values.
      else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
      else if (m3 !== undefined) a.push(m3);
      return ''; // Return empty string.
    });
  // Handle special case of empty last value.
  if (/,\s*$/.test(text)) a.push('');
  return a;
}

function runUtilFns(fnStr, dataItem) {
  let args = _.chain(fnStr)
    .replace(/^[A-Za-z0-9]+/, '') //to remove the function name
    .replace(/^\(|\)$/g, '') //to remove braces
    .thru(CSVtoArray)
    .value();

  let name = fnStr.split('(')[0];

  if (typeof DataItem.prototype[name] === 'function') {
    return dataItem[name].apply(dataItem, args);
  } else {
    return fnStr;
  }
}

function parseUtilFns(value, dataItem) {
  var re1 = /^([A-Za-z]+\([^\(\)]*\))$/g,
    re2 = /([A-Za-z]+\(.*?\))/g,
    matchArr,
    output = value;

  if (re1.test(value)) {
    return runUtilFns(value, dataItem);
  }

  while (matchArr = re2.exec(value)) {
    output = output.replace(matchArr[1], runUtilFns(matchArr[1], dataItem));
  }
  return output;
}

function parse(template, dataItem) {
  var output = {};

  _.forEach(template, function (value, key) {
    if (_.isString(value)) {
      value = _.trim(value);
      output[key] = parseUtilFns(value, dataItem);
    } else if (_.isFunction(value)) {
      output[key] = value.apply(dataItem);
    } else if (_.isPlainObject(value)) {
      output[key] = parse(value, dataItem);
    } else {
      output[key] = value
    }
  });

  return output;
}

function build(input) {
  let count = _.isInteger(input.count) ? input.count : 5,
    index = _.isInteger(input.index) ? input.index : 0,
    template = input.template;

  DataItem.indexRange = [index, index + count]; //This will be used to run sibling and siblings service method

  return _.map(_.range(index, index + count), (index) => parse(template, new DataItem(index)));
}

module.exports = build;