# JsonMocker - An extendable tool to generate mock json data.

## Usage

### Node.js

#### 1) Quick demo
    var mocker = require('json-mocker');

    var mockdata = mock.build({
      count: 50,
      index: 1000,
      template: {
        name: {
          first: 'firstName()',
          last: 'lastName()'
        },
        address: {
          street: 'street()',
          city: 'city()',
          state: 'state()'
        }
      }
    });

    //More examples in the examples directory

#### 2) Extend the library with a custom method using provider
    var mocker = require('json-mocker');

    mocker.provider.add({
      type: 'quality',
      data: ['learned', 'hardworking', 'careless', 'casual', 'professional']
    });

    jsonMocker.provider.add({
      type: 'color',
      data: [{"_id":"c001","name":"red","hex":"#f00"},{"_id":"c002","name":"green","hex":"#0f0"},{"_id":"c003","name":"blue","hex":"#00f"},{"_id":"c004","name":"cyan","hex":"#0ff"},{"_id":"c005","name":"magenta","hex":"#f0f"},{"_id":"c006","name":"yellow","hex":"#ff0"},{"_id":"c007","name":"black","hex":"#000"}]
    });

    var mockdata = mock.build({
      count: 50,
      index: 250,
      template: {
        name: 'firstName()',
        quality: 'quality()',
        color: 'color()',
        desc: function () {
          return 'firstName() has ' + this.quality() + ' quality. His/Her favorite color is: ' + this.util.getValueById('color', this.color()).name + '.';
        }
      }
    });

#### 3) Direct invocation of data service methods.

    var jsonMocker = require('../');

    var dataItem = jsonMocker.dataItem(20);

    console.log(dataItem.index());
    console.log(dataItem.email());
    console.log(dataItem.company());
    console.log(dataItem.salary());


    for (var i = 0; i < 10; i++) {
      dataItem = jsonMocker.dataItem(i);
      console.log(JSON.stringify({
        id: dataItem.index(),
        name: dataItem.firstName() + ' ' + dataItem.lastName(),
        gender: dataItem.gender(),
        ratings: dataItem.ratings(),
      }, null, 2));
    }

## API - base methods

### JsonMocker.build

### JsonMocker.provider.add

## API - data service methods

method | Usage | Description | Returns same value * |
--- | --- | --- | --- |
index|index()|Index of current data item.  It's starting number can be specified at root level using property index|true|
boolean|boolean()|Random boolean value - true or false|true|
date|date(fromYear, toYear, momentFormat)|fromYear and toYear should be a four-digit integer. The date format can be given in MomentJs formats (http://momentjs.com/docs/#/displaying/)|false|
integer|integer(topLimit)|topLimit should be a integer.  The random integer returned by this method, will be less than topLimit.|false|
gender|gender()|The tool sets a random gender for each data item.|true|
firstName|firstName()|Returns a random first name based on the set gender by gender()|true|
lastName|lastName()|Returns a random last name based on the set gender by gender()|true|
email|email()|Returns email based on firstName, lastName and company name|true|
street|street()|Returns a address with 3 parts - number, street part 1 and street part 2|true|
city|city()|Returns a random US city.  It belongs to the same state return by state()|true|
state|state()|Returns a random US state.|true|
company|company()|Returns a random company name.|true|
phone|phone()|Returns a 10 digit number|true|
lorem|lorem(format)|The format parameter should be a combination of integer (for count) and any one of the letter 'w', 's' or 'p' (for words, sentences and paragraphs respectively).  Examples: '10s', '38w', '5p'.  If not mentioned, count will default to 10 and text type default to words.|false|
sibling|sibling()|This returns one index value other than the current value. This can be used to create relationship among the data items|false|
siblings|siblings(max, noEmptyList)|This returns a list of index values excluding the current value. This can be used to create relationship among the data items.  The number of values in the list is random with `max` argument as upper limit.|false|

(*) - Returns same value for every use in a template.
