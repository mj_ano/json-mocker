'use strict';

let jsonMocker = require('../'), template = require('./templates/person-detailed-template');

var mockdata = jsonMocker.build({
  count: 5,
  index: 500,
  template: template
});

console.log(JSON.stringify(mockdata, null, 2));