'use strict';

let jsonMocker = require('../');

jsonMocker.provider.add({
  type: 'quality',
  data: ['learned', 'hardworking', 'careless', 'casual', 'professional']
});


jsonMocker.provider.add({
  type: 'color',
  data: [{"_id":"c001","name":"red","hex":"#f00"},{"_id":"c002","name":"green","hex":"#0f0"},{"_id":"c003","name":"blue","hex":"#00f"},{"_id":"c004","name":"cyan","hex":"#0ff"},{"_id":"c005","name":"magenta","hex":"#f0f"},{"_id":"c006","name":"yellow","hex":"#ff0"},{"_id":"c007","name":"black","hex":"#000"}]
});



var mockdata = jsonMocker.build({
  count: 5,
  index: 500,
  template: {
    name: 'firstName()',
    quality: 'quality()',
    favColor: 'color()',
    currency: 'currency()',
    desc: function () {
      return 'firstName() has ' + this.quality() + ' quality. His/Her favorite color is: ' + this.util.getValueById('color', this.color()).name + '.';
    }
  }
});

console.log(JSON.stringify(mockdata, null, 2));