'use strict';

let _ = require('lodash');

module.exports = {
  _id: 'index()',
  gender: 'gender()',
  introduction: {
    short: 'lastName(), firstName() - employee of company()',
    long: function () {
      let title = this.util.getValueById('title', this.title());

      return (this.gender() === 'M' ? 'Mr. ' : 'Ms. ') + this.firstName() + ' ' + this.lastName() + ' (' + this.index() + ')' + ' is currently working in ' + this.company() + ' as ' + title.value + '(' + this.title() + ')' + '.';
    }
  },
  name: {
    first: 'firstName()',
    last: 'lastName()'
  },
  personal: {
    picture: 'http://lorempixel.com/100/100',
    dateOfBirth: 'date(1970, 1992)',
    noOfTShirts: 'integer(35)'
  },
  address: {
    street: 'street()',
    city: 'city()',
    state: 'state()'
  },
  official: {
    company: 'company()',
    title: 'title()',
    salary: 'salary()',
    ratings: 'ratings()'
  },
  contact: {
    email: 'email()',
    phone: 'phone()'
  },
  socialMediaPresence: {
    facebook: 'boolean()',
    twitter: 'boolean()',
    whatsapp: 'boolean()'
  }
};
