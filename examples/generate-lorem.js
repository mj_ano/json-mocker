'use strict';

let jsonMocker = require('../');

let mockdata = jsonMocker.build({
  count: 10,
  template: {
    '10-word': 'lorem("10w")',
    '6-sentence': 'lorem("6s")',
    '3-paragragh': 'lorem("3p")'
  }
});

console.log(JSON.stringify(mockdata, null, 2));