'use strict';

let jsonMocker = require('../');

let dataItem = jsonMocker.dataItem(20);

console.log(dataItem.index());
console.log(dataItem.email());
console.log(dataItem.company());
console.log(dataItem.salary());


for (let i = 0; i < 10; i++) {
  dataItem = jsonMocker.dataItem(i);
  console.log(JSON.stringify({
    id: dataItem.index(),
    name: dataItem.firstName() + ' ' + dataItem.lastName(),
    gender: dataItem.gender(),
    ratings: dataItem.ratings(),
  }, null, 2));
}