'use strict';

let jsonMocker = require('../');

let mockdata = jsonMocker.build({
  count: 10,
  index: 500,
  template: {
    dob: 'date(1995, 2016, "dddd, MMMM Do YYYY, h:mm:ss a")',
    doj: 'date(2010, 2015, "YYYY-MM-DD")',
    dor: 'date(1995, 2000, "Do MMMM YYYY")',
    doe: 'date(1970, 1980, "LTS")',
    doq: 'date(1925, 1940, "LLL")',
    rand1: 'integer(1000)',
    rand2: 'integer(1000)',
    rand3: 'integer(1000)'
  }
});

console.log(JSON.stringify(mockdata, null, 2));